# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :less_impact,
  namespace: LessImpact,
  ecto_repos: [LessImpact.Repo]

# Configures the endpoint
config :less_impact, LessImpactWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "osddZUoMMNLi1Q7IuMDB/BnrWyhmUYrerOKcnXc+pcXei5DyQuN45K4kf71nJGfG",
  render_errors: [view: LessImpactWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: LessImpact.PubSub,
  live_view: [signing_salt: "2ZMw+7Qq"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configuration for additional template engines
config :phoenix, :template_engines,
  # PhoenixMarkdown (https://github.com/boydm/phoenix_markdown)
  md: PhoenixMarkdown.Engine

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
