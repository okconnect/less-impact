defmodule LessImpactWeb.PageLive do
  use LessImpactWeb, :live_view

  alias LessImpact.Calculator.CalculatorForm

  @impl true
  def mount(_params, _session, socket) do
    changeset =
      CalculatorForm.changeset(
        %{
          attendees: 1,
          hours: 1,
          sessions: 1
        }
      )

    {
      :ok,
      assign(
        socket,
        page_title: "Home",
        changeset: changeset,
        co2_footprint: calculate_co2_footprint("1", "1", "1")
      )
    }
  end

  @impl true
  def handle_event("validate", %{"calculator_form" => calculator_params}, socket) do
    IO.inspect(calculator_params)
    changeset = CalculatorForm.changeset(calculator_params)
    IO.inspect(changeset)

    %{
      "attendees" => attendees,
      "hours" => hours,
      "sessions" => sessions
    } = calculator_params

    co2_footprint =
      case changeset.valid? do
        true ->  calculate_co2_footprint(attendees, hours, sessions)
        _ -> nil
      end

    socket =
      socket
      |> assign(co2_footprint: co2_footprint)
      |> assign(changeset: %{changeset | action: :calc})
    {:noreply, socket}
  end

  defp calculate_co2_footprint("", _hours, _sessions), do: nil
  defp calculate_co2_footprint(_attendees, "", _sessions), do: nil
  defp calculate_co2_footprint(_attendees, _hours, ""), do: nil
  defp calculate_co2_footprint(attendees, hours, sessions) do

    {sessions, _} = Integer.parse(sessions)
    {attendees, _} = Integer.parse(attendees)
    {hours, _} = Float.parse(hours)

    %{
      min: calculate_co2_range(:min, attendees * hours * sessions),
      max: calculate_co2_range(:max, attendees * hours * sessions),
    }
  end

  defp calculate_co2_range(limit, total_hours) do
    case limit do
      :min -> (0.013 * total_hours)
      :max -> (0.204 * total_hours)
    end
    |> Number.Delimit.number_to_delimited()
  end
end
