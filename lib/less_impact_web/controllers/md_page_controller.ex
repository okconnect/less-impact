defmodule LessImpactWeb.MDPageController do
  use LessImpactWeb, :controller

  def about(conn, _params) do
    render(conn, "md_page.html", page: "about", page_title: "About")
  end

  def privacy_policy(conn, _params) do
    render(conn, "privacy_policy.html")
  end
end
