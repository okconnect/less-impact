defmodule LessImpact.Repo do
  use Ecto.Repo,
    otp_app: :less_impact,
    adapter: Ecto.Adapters.Postgres
end
