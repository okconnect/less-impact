defmodule LessImpact.Calculator.CalculatorForm do
  import Ecto.Changeset

  defstruct [:sessions, :attendees, :hours]

  @types %{
    sessions: :integer,
    attendees: :integer,
    hours: :float
  }

  def changeset(attrs \\ %{}) do
    {%__MODULE__{}, @types}
    |> cast(attrs, [:sessions, :attendees, :hours])
    |> validate_required([:sessions, :attendees, :hours], message: "You need to provide this information to get a result")
    |> validate_number(:sessions, greater_than: 0, message: "Must be greater than zero")
    |> validate_number(:attendees, greater_than: 0, message: "Must be greater than zero")
    |> validate_number(:hours, greater_than: 0, message: "Must be greater than zero")
  end
end
